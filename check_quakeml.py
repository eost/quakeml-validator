#! /usr/bin/env python3

import argparse
import logging
from math import floor, isclose
import os
import sys
import unittest

import numpy as np
from obspy import read_events
from obspy.io.quakeml.core import _validate as validate


def recursive_file_gen(mydir):
    if os.path.isfile(mydir):
        yield mydir
        return

    for root, dirs, files in os.walk(mydir):
        for f in files:
            if f.endswith('.xml'):
                yield os.path.join(root, f)


class SiHexTestCase(unittest.TestCase):
    def __init__(self, test_name, filename):
        super(SiHexTestCase, self).__init__(test_name)
        self.filename = filename
        self.catalog = read_events(self.filename)

    def shortDescription(self):
        return self.filename

    def setUp(self):
        self.event = self.catalog[0]

    def test_validate_quakeml(self):
        valid = validate(self.filename)
        self.assertTrue(valid)

    def test_event_nb(self):
        self.assertEqual(len(self.catalog.events), 1)

    def test_preferred_origin(self):
        origin = self.event.preferred_origin()
        self.assertIsNotNone(origin)

    def test_preferred_magnitude(self):
        magnitude = self.event.preferred_magnitude()
        self.assertIsNotNone(magnitude)

    def test_amplitude_pick(self):
        """
        Check that all amplitudes have a pick.
        """
        pick_ids = [a.pick_id for a in self.event.amplitudes]
        self.assertTrue(None not in pick_ids)

    def test_phase_count(self):
        """
        Check associatedPhaseCount and usedPhaseCount.
        """
        for origin in self.event.origins:
            self.assertEqual(
                len(origin.arrivals),
                origin.quality.associated_phase_count,
            )

            used_arrivals = [a for a in origin.arrivals if a.time_weight]
            self.assertEqual(
                len(used_arrivals),
                origin.quality.used_phase_count,
            )

    def test_station_count(self):
        """
        Check associatedStationCount and usedStationCount.
        """
        for origin in self.event.origins:
            stations = set()
            used_stations = set()
            for arrival in origin.arrivals:
                pick = arrival.pick_id.get_referred_object()
                code = '%s.%s' % (pick.waveform_id.network_code,
                                  pick.waveform_id.station_code)
                stations.add(code)

                if arrival.time_weight:
                    used_stations.add(code)

            self.assertEqual(
                len(stations),
                origin.quality.associated_station_count,
            )

            self.assertEqual(
                len(used_stations),
                origin.quality.used_station_count,
            )

    def test_standard_error(self):
        for origin in self.event.origins:
            res = np.array(
                [a.time_residual for a in origin.arrivals if a.time_weight]
            )
            rms = np.sqrt(np.mean(res**2))

            self.assertAlmostEqual(rms, origin.quality.standard_error)

    def test_azimuthal_gap(self):
        """
        Check origin azimuthal gap from arrival azimuths.
        """
        for origin in self.event.origins:
            azimuths = \
                [a.azimuth % 360 for a in origin.arrivals if a.time_weight]
            azimuths.sort()
            azimuths.append(azimuths[0] + 360)

            gaps = np.diff(azimuths)

            self.assertAlmostEqual(
                origin.quality.azimuthal_gap,
                floor(max(gaps)),
                places=0,
            )

    def test_distance(self):
        """
        Check origin min and max distance with arrival distances.
        """
        for origin in self.event.origins:
            distances = [a.distance for a in origin.arrivals if a.time_weight]

            self.assertAlmostEqual(
                origin.quality.minimum_distance,
                min(distances),
                places=0,
            )

            self.assertAlmostEqual(
                origin.quality.maximum_distance,
                max(distances),
                places=0,
            )

    def test_pick_nb(self):
        pick_ids = []
        for origin in self.event.origins:
            pick_ids += [a.pick_id.id for a in origin.arrivals]

        event_pick_ids = [o.resource_id.id for o in self.event.picks]
        self.assertEqual(
            len(set(pick_ids)),
            len(set(event_pick_ids)),
        )

    def test_stationmagnitude_nb(self):
        station_magnitude_count = 0

        for magnitude in self.event.magnitudes:
            smcs = magnitude.station_magnitude_contributions
            nbr_smc = len([smc for smc in smcs if not isclose(smc.weight, 0)])
            self.assertEqual(
                nbr_smc,
                magnitude.station_count or 0,
            )

            station_magnitudes = [
                smc.station_magnitude_id.get_referred_object()
                for smc in magnitude.station_magnitude_contributions
            ]
            station_magnitudes = \
                [sm for sm in station_magnitudes if sm is not None]

            station_magnitude_count += len(station_magnitudes)

        self.assertEqual(
            station_magnitude_count,
            len(self.event.station_magnitudes),
        )

    def test_event_description(self):
        for ed in self.event.event_descriptions:
            if ed is not None and ed.type is not None:
                self.assertIsNotNone(ed.text)

    def test_origin_duplication(self):
        origin_ids = [o.resource_id.id for o in self.event.origins]
        self.assertEqual(len(origin_ids), len(set(origin_ids)))

    def test_pick_duplication(self):
        pick_ids = [o.resource_id.id for o in self.event.picks]
        self.assertEqual(len(pick_ids), len(set(pick_ids)))

    def test_amplitude_duplication(self):
        amplitude_ids = [o.resource_id.id for o in self.event.amplitudes]
        self.assertEqual(len(amplitude_ids), len(set(amplitude_ids)))

    def test_magnitude_duplication(self):
        magnitude_ids = [o.resource_id.id for o in self.event.magnitudes]
        self.assertEqual(len(magnitude_ids), len(set(magnitude_ids)))

    def test_station_magnitude_duplication(self):
        sta_mag_ids = [o.resource_id.id for o in self.event.station_magnitudes]
        self.assertEqual(len(sta_mag_ids), len(set(sta_mag_ids)))

    def test_arrival_duplication(self):
        arrival_ids = \
            [a.resource_id.id for o in self.event.origins for a in o.arrivals]
        self.assertEqual(len(arrival_ids), len(set(arrival_ids)))


if __name__ == '__main__':
    all_tests = unittest.TestLoader().getTestCaseNames(SiHexTestCase)
 
    parser = argparse.ArgumentParser(description=("Check SiHex files"))
    parser.add_argument('directory',
                        help='Check all files of this directory')
    parser.add_argument('-t', '--tests',
                        help='Check only those given tests',
                        nargs='+')
    parser.add_argument('-v', '--verbose',
                        help=('Can be supplied multiple time to increase '
                              'verbosity (default: ERROR).'),
                        action='count',
                        default=0)

    args = parser.parse_args()
    directory = args.directory
    tests = args.tests or []
    verbose = args.verbose

    # Set logging level
    levels = [logging.ERROR, logging.WARNING, logging.INFO, logging.DEBUG]
    log_level = levels[min(len(levels) - 1, verbose)]
    log_format = '\033[2K%(asctime)s %(name)s: %(levelname)s: %(message)s'
    logging.basicConfig(level=log_level,
                        format=log_format,
                        datefmt="%Y-%m-%d %H:%M:%S")

    if tests:
        # Launch selected tests only
        tests = set.intersection(set(tests), set(all_tests))
    else:
        # Launch all tests by default
        tests = all_tests

    suite = unittest.TestSuite()
    for filename in recursive_file_gen(directory):
        logging.info('Add test file %s' % filename)

        for test in tests:
            suite.addTest(SiHexTestCase(test, filename))
    runner = unittest.TextTestRunner()
    runner.run(suite)