# QuakeML validator

This project aims to check QuakeML file and reduce consistency errors.

The script is quite simple so just look at it to see the details of the tests.

## Usage

The `check_quakeml.py` script can be run on a file or a directory:

```sh
./check_quakeml.py event.xml
```

By default every tests are executed. However, it may be more practical to run
only some selected tests.

```sh
./check_quakeml.py -t test_phase_count test_station_count event.xml
```
